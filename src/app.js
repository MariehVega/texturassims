/**
 * GLOBAL VARS
 */
lastTime = Date.now();
cameras = {
    default: null,
    current: null
};
canvas = {
    element: null,
    container: null
}
labels = {}
cameraControl = null;
stats = null;
scene = null;
renderer = null

/**
 * Function to start program running a
 * WebGL Application trouhg ThreeJS
 */
let webGLStart = () => {
    initScene();
    window.onresize = onWindowResize;
    lastTime = Date.now();
    animateScene();
};

/**
 * Here we can setup all our scene noobsters
 */
function initScene() {
    //Selecting DOM Elements, the canvas and the parent element.
    canvas.container = document.querySelector("#app");
    canvas.element = canvas.container.querySelector("#appCanvas");

    /**
     * SETTING UP CORE THREEJS APP ELEMENTS (Scene, Cameras, Renderer)
     * */
    scene = new THREE.Scene();

    renderer = new THREE.WebGLRenderer({ canvas: canvas.element });
    renderer.setSize(canvas.container.clientWidth, canvas.container.clientHeight);
    renderer.setClearColor(0x2c3e50, 1);

    canvas.container.appendChild(renderer.domElement);

    //positioning cameras
    cameras.default = new THREE.PerspectiveCamera(45, canvas.container.clientWidth / canvas.container.clientHeight, 0.1, 100000);
    cameras.default.position.set(0, 2000, 4000);
    cameras.default.lookAt(new THREE.Vector3(0, 0, 0));
    //Setting up current default camera as current camera
    cameras.current = cameras.default;
    //Camera control Plugin
    cameraControl = new THREE.OrbitControls(cameras.current, renderer.domElement);

    //estadisticas
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '10px';
    stats.domElement.style.left = '10px';
    stats.domElement.style.zIndex = '100';
    document.body.appendChild(stats.domElement);

    /*****
     * CONFIGURACIÓN DE ELEMENTOS DE LA ESCENA FUERA DE LAS COSAS DE SIEMPRE...
     * (CAMARA, ESCENA, ESTADÍSTICAS). ACÁ VA LO NUEVO!
     */
    //Luz AMbiente
    var luzAmb = new THREE.AmbientLight(0xFFFFFF);
    scene.add(luzAmb);

    //Construyendo el suelo

    var suelo = new THREE.Mesh(
        new THREE.BoxGeometry(8640, 200, 6480, 13, 1, 10),
        new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture('assets/sims/suelo.png'),
            depthTest: false
            //wireframe: true
        })
    );

    uvSuelo1 = [
        new THREE.Vector2(0, 1),
        new THREE.Vector2(0, 0),
        new THREE.Vector2(0.33, 0),
        new THREE.Vector2(0.33, 1)
    ];
    uvSuelo2 = [
        new THREE.Vector2(0.33, 1),
        new THREE.Vector2(0.33, 0),
        new THREE.Vector2(0.66, 0),
        new THREE.Vector2(0.66, 1)
    ];
    uvSuelo3 = [
        new THREE.Vector2(0.68, 1),
        new THREE.Vector2(0.68, 0),
        new THREE.Vector2(1, 0),
        new THREE.Vector2(1, 1)
    ];
    

    
    suelo.geometry.faceVertexUvs[0] = [];

    for (let i = 0, j = 1, k = 0, l=0; k < 948; i = i + 2, j = j + 2, k++) {
        if (k>19 && k<150) {
            if (l>12) {
                l=0;
            }
            if (l<9) {
                suelo.geometry.faceVertexUvs[0][i] = [
                    uvSuelo1[0],
                    uvSuelo1[1],
                    uvSuelo1[3]
                ];
                suelo.geometry.faceVertexUvs[0][j] = [
                    uvSuelo1[1],
                    uvSuelo1[2],
                    uvSuelo1[3]
                ];
            } else {
                if (k % 2 == 0) {
                    suelo.geometry.faceVertexUvs[0][i] = [
                        uvSuelo2[0],
                        uvSuelo2[1],
                        uvSuelo2[3]
                    ];
                    suelo.geometry.faceVertexUvs[0][j] = [
                        uvSuelo2[1],
                        uvSuelo2[2],
                        uvSuelo2[3]
                    ];
                } else {
                    suelo.geometry.faceVertexUvs[0][i] = [
                        uvSuelo2[3],
                        uvSuelo2[0],
                        uvSuelo2[2]
                    ];
                    suelo.geometry.faceVertexUvs[0][j] = [
                        uvSuelo2[0],
                        uvSuelo2[1],
                        uvSuelo2[2]
                    ];
                }
            }
            l++;
        }else{
            suelo.geometry.faceVertexUvs[0][i] = [
                uvSuelo3[3],
                uvSuelo3[0],
                uvSuelo3[2]
            ];
            suelo.geometry.faceVertexUvs[0][j] = [
                uvSuelo3[0],
                uvSuelo3[1],
                uvSuelo3[2]
            ];
        }
        
    }


    //Construyendo paredes y ventanas

    var material2 = new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture('assets/sims/paredes.png'),
        transparent: true
        //wireframe: true
    });

    var pared1 = new THREE.Mesh(
        new THREE.BoxGeometry(2872, 3240, 200),
        material2
    );

    var puerta1 = new THREE.Mesh(
        new THREE.BoxGeometry(1080, 2160, 100),
        material2
    );

    var pared2 = new THREE.Mesh(
        new THREE.BoxGeometry(2528, 3240, 200),
        material2
    );

    var ventana1 = new THREE.Mesh(
        new THREE.BoxGeometry(1120, 1480, 100),
        material2
    );

    var pared3 = new THREE.Mesh(
        new THREE.BoxGeometry(1440, 3240, 200),
        material2
    );

    var pared4 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 3240, 2500),
        material2
    );

    var ventana2 = new THREE.Mesh(
        new THREE.BoxGeometry(100, 1480, 1120),
        material2
    );

    var pared5 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 3240, 2860),
        material2
    );

    var pared6 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 1080, 6480,1,1,5),
        material2
    );

    var pared7 = new THREE.Mesh(
        new THREE.BoxGeometry(2872, 1080, 200),
        material2
    );

    var pared8 = new THREE.Mesh(
        new THREE.BoxGeometry(2528, 1080, 200),
        material2
    );

    var pared9 = new THREE.Mesh(
        new THREE.BoxGeometry(1440, 1080, 200),
        material2
    );

    var pared10 = new THREE.Mesh(
        new THREE.BoxGeometry(1080, 200, 200),
        material2
    );

    var pared11 = new THREE.Mesh(
        new THREE.BoxGeometry(1120, 550, 200),
        material2
    );

    var pared12 = new THREE.Mesh(
        new THREE.BoxGeometry(1120, 550, 200),
        material2
    );

    var pared13 = new THREE.Mesh(
        new THREE.BoxGeometry(1080, 550, 200),
        material2
    );

    var pared14 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 550, 1120),
        material2
    );

    var pared15 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 1080, 2860),
        material2
    );

    var pared16 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 1080, 2860),
        material2
    );

    var pared17 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 1080, 1300),
        material2
    );

    var pared18 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 1080, 1300),
        material2
    );

    var pared19 = new THREE.Mesh(
        new THREE.BoxGeometry(200, 1080, 2000),
        material2
    );

    var pared20 = new THREE.Mesh(
        new THREE.BoxGeometry(2860, 1080, 200),
        material2
    );

    var puerta2 = new THREE.Mesh(
        new THREE.BoxGeometry(100, 2160, 1080),
        material2
    );

    var puerta3 = new THREE.Mesh(
        new THREE.BoxGeometry(100, 2160, 1080),
        material2
    );

    var pared21 = new THREE.Mesh(
        new THREE.BoxGeometry(800, 1080, 200),
        material2
    );

    var puerta4 = new THREE.Mesh(
        new THREE.BoxGeometry(1080, 2160, 100),
        material2
    );

    var puerta5 = new THREE.Mesh(
        new THREE.BoxGeometry(1080, 2160, 100),
        material2
    );

    var pared22 = new THREE.Mesh(
        new THREE.BoxGeometry(800, 1080, 200),
        material2
    );

    //Definiendo segmentos

    //Ladrillos
    uvPared1 = [
        new THREE.Vector2(0, 1),
        new THREE.Vector2(0, 0.6),
        new THREE.Vector2(0.53, 0.6),
        new THREE.Vector2(0.53, 1)
    ];
    //Ladrillos,Pequeños
    uvPared11 = [
        new THREE.Vector2(0, 0.73),
        new THREE.Vector2(0, 0.6),
        new THREE.Vector2(0.2, 0.6),
        new THREE.Vector2(0.2, 0.73)
    ];
    //Ladrillos,Vertical
    uvPared12 = [
        new THREE.Vector2(0, 1),
        new THREE.Vector2(0, 0.6),
        new THREE.Vector2(0.06, 0.6),
        new THREE.Vector2(0.06, 1)
    ];
    //Ladrillos,Horizontal
    uvPared13 = [
        new THREE.Vector2(0, 0.66),
        new THREE.Vector2(0, 0.6),
        new THREE.Vector2(0.2, 0.6),
        new THREE.Vector2(0.2, 0.66)
    ];
    //Ladrillos,Vertical
    uvPared14 = [
        new THREE.Vector2(0, 0.73),
        new THREE.Vector2(0, 0.6),
        new THREE.Vector2(0.06, 0.6),
        new THREE.Vector2(0.06, 0.73)
    ];
    //Azul
    uvPared2 = [
        new THREE.Vector2(0.45, 0.6),
        new THREE.Vector2(0.45, 0),
        new THREE.Vector2(0.5, 0),
        new THREE.Vector2(0.5, 0.6)
    ];
    //Gris
    uvPared3 = [
        new THREE.Vector2(0.53, 0.13),
        new THREE.Vector2(0.53, 0),
        new THREE.Vector2(1, 0),
        new THREE.Vector2(1, 0.13)
    ];
    //Ventana
    uvVentana = [
        new THREE.Vector2(0, 0.53),
        new THREE.Vector2(0, 0),
        new THREE.Vector2(0.4, 0),
        new THREE.Vector2(0.4, 0.53)
    ];
    //Puerta
    uvPuerta = [
        new THREE.Vector2(0.53, 1),
        new THREE.Vector2(0.53, 0.13),
        new THREE.Vector2(1, 0.13),
        new THREE.Vector2(1, 1)
    ];

    var paPuVe = [pared1,pared2,pared3,pared4,pared5,pared6,pared7,pared8,pared9,pared10,
    pared11,pared12,pared13,pared14,pared15,pared16,pared17,pared18,pared19,pared20,pared21,
    pared22,puerta1,puerta2,puerta3,puerta4,puerta5,ventana1,ventana2];

    for (let i = 0; i < paPuVe.length; i++) {
        paPuVe[i].geometry.faceVertexUvs[0] = [];
    }

    //Pintando

    for (let i = 27; i < paPuVe.length; i++) {
        if (i==27) {
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvVentana[0],
                uvVentana[1],
                uvVentana[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvVentana[1],
                uvVentana[2],
                uvVentana[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvVentana[3],
                uvVentana[2],
                uvVentana[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvVentana[2],
                uvVentana[1],
                uvVentana[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
        }else{
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvVentana[0],
                uvVentana[1],
                uvVentana[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvVentana[1],
                uvVentana[2],
                uvVentana[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvVentana[3],
                uvVentana[2],
                uvVentana[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvVentana[2],
                uvVentana[1],
                uvVentana[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
        }
    }

    for (let i = 22; i < 27; i++) {
        if (i==22 || i>24) {
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPuerta[0],
                uvPuerta[1],
                uvPuerta[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPuerta[1],
                uvPuerta[2],
                uvPuerta[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPuerta[3],
                uvPuerta[2],
                uvPuerta[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPuerta[2],
                uvPuerta[1],
                uvPuerta[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
        }else{
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvPuerta[0],
                uvPuerta[1],
                uvPuerta[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvPuerta[1],
                uvPuerta[2],
                uvPuerta[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvPuerta[3],
                uvPuerta[2],
                uvPuerta[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvPuerta[2],
                uvPuerta[1],
                uvPuerta[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared3[3],
                uvPared3[2],
                uvPared3[0]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared3[2],
                uvPared3[1],
                uvPared3[0]
            ];
            
            
        }
    }

    for (let i = 0; i < 22; i++) {
        if (i<3) {
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPared1[0],
                uvPared1[1],
                uvPared1[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPared1[1],
                uvPared1[2],
                uvPared1[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvPared12[0],
                uvPared12[1],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvPared12[1],
                uvPared12[2],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvPared12[0],
                uvPared12[1],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvPared12[1],
                uvPared12[2],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared12[3],
                uvPared12[0],
                uvPared12[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared12[0],
                uvPared12[1],
                uvPared12[2]
            ];
        }
        if (i>=3 && i<5) {
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvPared1[0],
                uvPared1[1],
                uvPared1[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvPared1[1],
                uvPared1[2],
                uvPared1[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared12[0],
                uvPared12[1],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared12[1],
                uvPared12[2],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared12[0],
                uvPared12[1],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared12[1],
                uvPared12[2],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPared12[0],
                uvPared12[1],
                uvPared12[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPared12[1],
                uvPared12[2],
                uvPared12[3]
            ];
        }
        if (i==5) {
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][12] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][13] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][14] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][15] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][16] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][17] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][18] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][19] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvPared11[0],
                uvPared11[1],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvPared11[1],
                uvPared11[2],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvPared11[0],
                uvPared11[1],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvPared11[1],
                uvPared11[2],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared11[0],
                uvPared11[1],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared11[1],
                uvPared11[2],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][6] = [
                uvPared11[0],
                uvPared11[1],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][7] = [
                uvPared11[1],
                uvPared11[2],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared11[0],
                uvPared11[1],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared11[1],
                uvPared11[2],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][20] = [
                uvPared13[3],
                uvPared13[0],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][21] = [
                uvPared13[0],
                uvPared13[1],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][22] = [
                uvPared13[3],
                uvPared13[0],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][23] = [
                uvPared13[0],
                uvPared13[1],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][24] = [
                uvPared13[3],
                uvPared13[0],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][25] = [
                uvPared13[0],
                uvPared13[1],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][26] = [
                uvPared13[3],
                uvPared13[0],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][27] = [
                uvPared13[0],
                uvPared13[1],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][28] = [
                uvPared13[3],
                uvPared13[0],
                uvPared13[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][29] = [
                uvPared13[0],
                uvPared13[1],
                uvPared13[2]
            ];
        }
        if (i>5 && i<9) {
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared11[0],
                uvPared11[1],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared11[1],
                uvPared11[2],
                uvPared11[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvPared14[0],
                uvPared14[1],
                uvPared14[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvPared14[1],
                uvPared14[2],
                uvPared14[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvPared14[0],
                uvPared14[1],
                uvPared14[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvPared14[1],
                uvPared14[2],
                uvPared14[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared12[3],
                uvPared12[0],
                uvPared12[2]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared12[0],
                uvPared12[1],
                uvPared12[2]
            ];
        }
        if (i>=9 && i<13) {
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared3[0],
                uvPared3[1],
                uvPared3[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared3[1],
                uvPared3[2],
                uvPared3[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPared3[0],
                uvPared3[1],
                uvPared3[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPared3[1],
                uvPared3[2],
                uvPared3[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared3[0],
                uvPared3[1],
                uvPared3[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared3[1],
                uvPared3[2],
                uvPared3[3]
            ];
        }
        if (i>=13) {
            paPuVe[i].geometry.faceVertexUvs[0][0] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][1] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][2] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][3] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][4] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][5] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][8] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][9] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][10] = [
                uvPared2[0],
                uvPared2[1],
                uvPared2[3]
            ];
            paPuVe[i].geometry.faceVertexUvs[0][11] = [
                uvPared2[1],
                uvPared2[2],
                uvPared2[3]
            ];
        }
    }

    //Ubicación de las ventanas y puertas

    puerta1.position.x += -1110;
    puerta1.position.y += 1180;
    puerta1.position.z += -3290;
    ventana1.position.x += 2510;
    ventana1.position.y += 1180;
    ventana1.position.z += -3290;
    ventana2.position.x += -4370;
    ventana2.position.y += 1180;
    ventana2.position.z += -180;
    puerta2.position.x += 1640;
    puerta2.position.y += 1180;
    puerta2.position.z += -170;
    puerta3.position.x += 1640;
    puerta3.position.y += 1180;
    puerta3.position.z += -1450;
    puerta4.position.x += 200;
    puerta4.position.y += 1180;
    puerta4.position.z += -2100;
    puerta5.position.x += -3050;
    puerta5.position.y += 1180;
    puerta5.position.z += -1350;

    //Ubicación de las paredes

    pared1.position.x += -3084;
    pared1.position.y += 1520;
    pared1.position.z += -3340;
    pared2.position.x += 690;
    pared2.position.y += 1520;
    pared2.position.z += -3340;
    pared3.position.x += 3790;
    pared3.position.y += 1520;
    pared3.position.z += -3340;
    pared4.position.y += 1520;
    pared4.position.x += -4420;
    pared4.position.z += -1990;
    pared5.position.y += 1520;
    pared5.position.x += -4420;
    pared5.position.z += 1800;
    pared6.position.y += 440;
    pared6.position.x += 4420;
    pared7.position.x += -3084;
    pared7.position.y += 440;
    pared7.position.z += 3340;
    pared8.position.x += 690;
    pared8.position.y += 440;
    pared8.position.z += 3340;
    pared9.position.x += 3790;
    pared9.position.y += 440;
    pared9.position.z += 3340;
    pared10.position.x += -1110;
    pared10.position.z += -3340;
    pared11.position.x += 2510;
    pared11.position.y += 170;
    pared11.position.z += -3340;
    pared12.position.x += 2510;
    pared12.position.y += 170;
    pared12.position.z += 3340;
    pared13.position.x += -1110;
    pared13.position.y += 170;
    pared13.position.z += 3340;
    pared14.position.x += -4420;
    pared14.position.y += 170;
    pared14.position.z += -180;
    pared15.position.y += 420;
    pared15.position.x += -2420;
    pared15.position.z += 1800;
    pared16.position.y += 420;
    pared16.position.x += 1640;
    pared16.position.z += 1800;
    pared17.position.y += 420;
    pared17.position.x += 1640;
    pared17.position.z += -2640;
    pared18.position.y += 420;
    pared18.position.x += -440;
    pared18.position.z += -2640;
    pared19.position.y += 420;
    pared19.position.x += -2420;
    pared19.position.z += -2240;
    pared20.position.x += 2980;
    pared20.position.y += 420;
    pared20.position.z += -810;
    pared21.position.x += -3980;
    pared21.position.y += 440;
    pared21.position.z += -1300;
    pared22.position.x += 1150;
    pared22.position.y += 440;
    pared22.position.z += -2090;

    //Mostrando
    scene.add(pared22,pared21,puerta5,puerta4,puerta2,pared20,puerta3,suelo,pared15,pared16,pared17,pared18,pared19,pared1,pared2,pared3,pared4,pared5,pared6,pared7,pared8,pared9,pared10,pared11,pared12,pared13,pared14,ventana1,ventana2,puerta1);

    //Contruyendo un sofá

    var material2 = new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture('assets/sims/cuero.png'),
        //transparent: true
        //wireframe: true
    });

    var material3 = new THREE.MeshPhongMaterial({
        map: THREE.ImageUtils.loadTexture('assets/sims/cuero2.png'),
        //transparent: true
        //wireframe: true
    });

    var pata1 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata2 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata3 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata4 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata5 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata6 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata7 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata8 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata9 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata10 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var pata11 = new THREE.Mesh(
        new THREE.BoxGeometry(100,100,100),
        material2
    );
    var muebleBase = new THREE.Mesh(
        new THREE.BoxGeometry(800,200,2000),
        material2
    );
    var muebleBase2 = new THREE.Mesh(
        new THREE.BoxGeometry(1500,200,800),
        material2
    );
    var espaldar1 = new THREE.Mesh(
        new THREE.BoxGeometry(100,800,2000),
        material2
    );
    var espaldar2 = new THREE.Mesh(
        new THREE.BoxGeometry(2200,800,100),
        material2
    );
    var cojin1 = new THREE.Mesh(
        new THREE.BoxGeometry(1000,300,700),
        material3
    );
    var cojin1 = new THREE.Mesh(
        new THREE.BoxGeometry(1100,300,700),
        material3
    );
    var cojin2 = new THREE.Mesh(
        new THREE.BoxGeometry(1100,300,700),
        material3
    );
    var cojin3 = new THREE.Mesh(
        new THREE.BoxGeometry(700,300,1200),
        material3
    );

    //Acomodando texturas

    uvCuero2 = [
        new THREE.Vector2(0, 1),
        new THREE.Vector2(0, 0),
        new THREE.Vector2(1, 0),
        new THREE.Vector2(1, 1)
    ];

    cojin1.geometry.faceVertexUvs[0]=[];
    cojin2.geometry.faceVertexUvs[0]=[];
    cojin3.geometry.faceVertexUvs[0]=[];

    cojin1.geometry.faceVertexUvs[0][8] = [
        uvCuero2[3],
        uvCuero2[2],
        uvCuero2[0]
    ];
    cojin1.geometry.faceVertexUvs[0][9] = [
        uvCuero2[2],
        uvCuero2[1],
        uvCuero2[0]
    ];
    cojin1.geometry.faceVertexUvs[0][2] = [
        uvCuero2[3],
        uvCuero2[2],
        uvCuero2[0]
    ];
    cojin1.geometry.faceVertexUvs[0][3] = [
        uvCuero2[2],
        uvCuero2[1],
        uvCuero2[0]
    ];
    cojin1.geometry.faceVertexUvs[0][4] = [
        uvCuero2[3],
        uvCuero2[2],
        uvCuero2[0]
    ];
    cojin1.geometry.faceVertexUvs[0][5] = [
        uvCuero2[2],
        uvCuero2[1],
        uvCuero2[0]
    ];

    cojin2.geometry.faceVertexUvs[0][4] = [
        uvCuero2[0],
        uvCuero2[1],
        uvCuero2[3]
    ];
    cojin2.geometry.faceVertexUvs[0][5] = [
        uvCuero2[1],
        uvCuero2[2],
        uvCuero2[3]
    ];
    cojin2.geometry.faceVertexUvs[0][8] = [
        uvCuero2[0],
        uvCuero2[1],
        uvCuero2[3]
    ];
    cojin2.geometry.faceVertexUvs[0][9] = [
        uvCuero2[1],
        uvCuero2[2],
        uvCuero2[3]
    ];

    cojin3.geometry.faceVertexUvs[0][4] = [
        uvCuero2[0],
        uvCuero2[3],
        uvCuero2[1]
    ];
    cojin3.geometry.faceVertexUvs[0][5] = [
        uvCuero2[3],
        uvCuero2[2],
        uvCuero2[1]
    ];
    cojin3.geometry.faceVertexUvs[0][2] = [
        uvCuero2[0],
        uvCuero2[1],
        uvCuero2[3]
    ];
    cojin3.geometry.faceVertexUvs[0][3] = [
        uvCuero2[1],
        uvCuero2[2],
        uvCuero2[3]
    ];
    cojin3.geometry.faceVertexUvs[0][8] = [
        uvCuero2[0],
        uvCuero2[1],
        uvCuero2[3]
    ];
    cojin3.geometry.faceVertexUvs[0][9] = [
        uvCuero2[1],
        uvCuero2[2],
        uvCuero2[3]
    ];


    //Ubicación del sofá

    pata1.position.x += 950;
    pata1.position.y += 150;
    pata1.position.z += 700;
    pata2.position.x += 950;
    pata2.position.y += 150;
    pata2.position.z += 1600;
    pata3.position.x += 950;
    pata3.position.y += 150;
    pata3.position.z += 2550;
    pata4.position.x += 300;
    pata4.position.y += 150;
    pata4.position.z += 700;
    pata5.position.x += -400;
    pata5.position.y += 150;
    pata5.position.z += 700;
    pata6.position.x += -1050;
    pata6.position.y += 150;
    pata6.position.z += 700;
    pata7.position.x += 300;
    pata7.position.y += 150;
    pata7.position.z += 1250;
    pata8.position.x += -400;
    pata8.position.y += 150;
    pata8.position.z += 1250;
    pata9.position.x += -1050;
    pata9.position.y += 150;
    pata9.position.z += 1250;
    pata10.position.x += 400;
    pata10.position.y += 150;
    pata10.position.z += 1600;
    pata11.position.x += 400;
    pata11.position.y += 150;
    pata11.position.z += 2550;
    muebleBase.position.x += 700;
    muebleBase.position.y += 300;
    muebleBase.position.z += 1600;
    muebleBase2.position.x += -450;
    muebleBase2.position.y += 300;
    muebleBase2.position.z += 1000;
    espaldar1.position.x += 1050;
    espaldar1.position.y += 750;
    espaldar1.position.z += 1600;
    espaldar2.position.x += -100;
    espaldar2.position.y += 750;
    espaldar2.position.z += 650;
    cojin1.position.x += -600;
    cojin1.position.y += 550;
    cojin1.position.z += 975;
    cojin2.position.x += 500;
    cojin2.position.y += 550;
    cojin2.position.z += 975;
    cojin3.position.x += 650;
    cojin3.position.y += 550;
    cojin3.position.z += 1920;

    //Mostrando

    scene.add(cojin3,cojin1,cojin2,espaldar1,espaldar2,pata1,pata2,pata3,pata4,pata5,pata6,pata7,pata8,pata9,pata10,pata11,muebleBase,muebleBase2);

}

/**
 * Function to render application over
 * and over.
 */
function animateScene() {
    requestAnimationFrame(animateScene);
    renderer.render(scene, cameras.current);
    updateScene();
}

/**
 * Function to evaluate logic over and
 * over again.
 */
function updateScene() {
    lastTime = Date.now();

    //Updating camera view by control inputs
    cameraControl.update();
    //Updating stats
    stats.update();

    //Updating labels in scene to look at selected camera
    for (const label of Object.keys(labels)) {
        labels[label].lookAt(cameras.current.position);
    }

}

function onWindowResize() {
    cameras.current.aspect = window.innerWidth / window.innerHeight;
    cameras.current.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}